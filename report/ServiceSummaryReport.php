<?php
  $FromDateDisplay =  $_GET['FromDate'];
  $ToDateDisplay =  $_GET['ToDate'];
  $FromDate =  "'" . $_GET['FromDate'] . "'";
  $ToDate = "'". $_GET['ToDate'] . "'";
  $reportby = $_GET['reportby'];
  //$ProductId = $_GET['ProductId'];

  if ($_GET['producttype_id'] != '') {
    $producttype_id = 'AND producttype_id = '.$_GET['producttype_id'].'';
  }else{
    $producttype_id = '';
  }

  $con = mysqli_connect("localhost","root","","spa_pos");
  $con->set_charset("utf8");
  $dbcon = mysqli_query($con,"SELECT * FROM servicesummaryreport 
                        WHERE CAST(CreateDocument AS DATE) BETWEEN CAST($FromDate AS DATE) AND CAST($ToDate  AS DATE) 
                         $producttype_id 
                        Order by Item_Qty DESC 
                        ");
  
  require_once('../TCPDF/tcpdf.php');
  class MYPDF extends TCPDF {

      //Page header
      public function Header() {
          $this->SetFont('angsanaupc', 'B', 16);
          $tDate=date('d/m/Y');
          $this->Cell(190, 10, 'Date '.$tDate, 0, 0, 'R');
      }

       public function Footer() {
          $this->SetFont('angsanaupc', 'B', 16);
          
          $this->Cell(0, 0, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, 0, 'C');
       }

  }

  // create new PDF document
  $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  // remove default header/footer
  $pdf->setHeaderData();
  $pdf->setFooterData();
  $pdf->setPrintHeader(true);
  $pdf->setPrintFooter(true);

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  // set margins
  $pdf->SetMargins(10, 10, 10,10);
  $pdf->SetHeaderMargin(10);
  $pdf->SetFooterMargin(10);

  // set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

  // set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  // set some language-dependent strings (optional)
  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }

  $pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

  // set font
  $pdf->SetFont('angsanaupc', '', 18);
  $pdf->AddPage('P', 'A4');
  $pdf->setPage(true);

  list($FromYear,$FromMonth,$FromDay) = split('-',$FromDateDisplay);
  list($ToYear,$ToMonth,$ToDay) = split('-',$ToDateDisplay);

  $theader = '<h1>Service Summary Report</h1>';
  $theader .= '<table border="0" style="padding-bottom:20px">';
  $theader .= '<tr style="">';
  $theader .= '<th colspan="7" align="">';
  $theader .= 'Report By : '.$reportby.' ';
  $theader .= '</th>';      
  $theader .= '<th colspan="5" align="right">';
  $theader .= 'Report Date : '.$FromDay .'/'. $FromMonth.'/'. $FromYear.' - '.$ToDay .'/'. $ToMonth.'/'. $ToYear.'';
  $theader .= '</th>';
  $theader .= '</tr>';
  $theader .= '</table>';
  $theader .= '</br>';
  $theader .= '<table border="1">';
  $theader .= '<tr style="">';
  $theader .= '<th colspan="1" align="center">';
  $theader .= '<b>No.</b>';
  $theader .= '</th>';   
   $theader .= '<th colspan="4" align="center">';
  $theader .= '<b>Product Name</b>';
  $theader .= '</th>'; 
  $theader .= '<th colspan="3" align="center">';
  $theader .= '<b>Sevice Name</b>';
  $theader .= '</th>';
  $theader .= '<th colspan="2" align="center">';
  $theader .= '<b>Qty</b>';
  $theader .= '</th>';
  $theader .= '<th colspan="2" align="center">';
  $theader .= '<b>Total</b>';
  $theader .= '</th>';
  $theader .= '</tr>';
  $i = 1;
  $Total_qty = 0;
  $Total_money = 0;
  while($row = mysqli_fetch_array($dbcon))
  {
      $CreateDocument = $row['CreateDocument'];
      $Item_Qty = number_format((float)$row['Item_Qty'], 2, '.', ',');
      $Item_Total = number_format((float)$row['Item_Total'], 3, '.', ',');
      $ProductType =$row['producttype_name'];
      $product_name = $row['product_name'];
      $theader .= '<tr style="">';
      $theader .= '<td colspan="1" align="center">';
      $theader .= ''.$i.'';
      $theader .= '</td>';
      $theader .= '<td colspan="4">';
      $theader .= ' '.$product_name.'';
      $theader .= '</td>';
      $theader .= '<td colspan="3">';
      $theader .= ' '.$ProductType.'';
      $theader .= '</td>';
      $theader .= '<td colspan="2"  align="right">';
      $theader .= ''.$Item_Qty.' <span> </span>';
      $theader .= '</td>';
      $theader .= '<td colspan="2"  align="right">';
      $theader .= ''.$Item_Total.' <span> </span>';
      $theader .= '</td>';
      $theader .= '</tr>';
      $Total_qty += $Item_Qty;
      $Total_money += $row['Item_Total'];
      $i++;
      
  }

    $theader .= '<tr style="">';
    $theader .= '<td colspan="8" align="center">';
    $theader .= '<b>Total</b>';
    $theader .= '</td>';
    $theader .= '<td colspan="2"  align="right">';
    $theader .= '<b>'.number_format((float)$Total_qty, 2, '.', ',').'</b> <span> </span>';
    $theader .= '</td>';
    $theader .= '<td colspan="2"  align="right">';
    $theader .= '<b>'.number_format((float)$Total_money, 3, '.', ',').'</b>  <span> </span>';
    $theader .= '</td>';
    $theader .= '</tr>';

  // $realrow = mysqli_num_rows($dbcon);
  // $faderow = 28;
  // $genrow = $faderow - $realrow;

  // for ($n=0; $n < $genrow ; $n++) { 
  //   $theader .= '<tr style="">';
  //   $theader .= '<td colspan="1" align="center">';
  //   $theader .= '';
  //   $theader .= '</td>';
  //   $theader .= '<td colspan="7">';
  //   $theader .= '';
  //   $theader .= '</td>';
  //   $theader .= '<td colspan="4"  align="right">';
  //   $theader .= '';
  //   $theader .= '</td>';
  //   $theader .= '</tr>';
  // }

  $theader .= '</table>';
  $tfooter = '';
  // Print text using writeHTMLCell()
  $pdf->writeHTML($theader.$tfooter, true, false, false, false, '');
  // ---------------------------------------------------------
  //Close and output PDF document
  $date = date("d/m/Y");
  list($d_o,$m_o,$Y_o) = split('/',$date);
  $Y_o = $Y_o + 543;
  $pdf->Output(''.$d_o.'-'.$m_o.'-'.$Y_o.'.pdf', 'I');
  mysqli_close($con);

?>
