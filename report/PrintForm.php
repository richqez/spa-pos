<?php
  $orderId = $_GET['orderId'];
  $reportby = $_GET['reportby'];
  $con = mysqli_connect("localhost","root","","spa_pos");
  $con->set_charset("utf8");
  $dbcon = mysqli_query($con,"SELECT * FROM printfrom WHERE order_id = $orderId
                            
                        ");

  require_once('../TCPDF/tcpdf.php');
  class MYPDF extends TCPDF {

      //Page header
      public function Header() {
          $this->SetFont('angsanaupc', 'B', 16);
          $tDate=date('d/M/Y');
         // $this->Cell(190, 10, 'วันที่ '.$tDate, 0, 0, 'R');
      }

       public function Footer() {
          $this->SetFont('angsanaupc', 'B', 16);
          
          $this->Cell(180, 0, 'หน้า '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, 0, 'L');
       }

  }

  // create new PDF document
  $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  // remove default header/footer
  $pdf->setHeaderData();
  $pdf->setFooterData();
  $pdf->setPrintHeader(true);
  $pdf->setPrintFooter(false);

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  // set margins
  $pdf->SetMargins(10, 10, 10,10);
  $pdf->SetHeaderMargin(10);
  $pdf->SetFooterMargin(false);

  // set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

  // set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  // set some language-dependent strings (optional)
  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }

  $pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

  // set font
  $pdf->SetFont('angsanaupc', '', 18);
  $pdf->AddPage('P', 'A4');
  $pdf->setPage(true);



  $theader = '';
  $theader .= '<table border="0" style="padding-bottom:20px">';
  $theader .= '<tr style="">';
  $theader .= '<th colspan="7" align="center">';
  $theader .= '<h1 style="font-size:50px;vertical-align:top;">Ann <span style="color:orange">Spa</span></h1>';
  $theader .= '</th>';      
  $theader .= '<th colspan="5" align="">';
  $theader .= '<span style="line-height: 80%;">  AnnSPA <br>
              Phone Number(830) 935-4747 <br>
              Address23046 N Cranes Mill Rd, Canyon <br>
              Lake, TX, 78133</span>';
  $theader .= '</th>';
  $theader .= '</tr>';
  $theader .= '</table>';

  $headercon = mysqli_query($con,"SELECT * FROM printfrom  WHERE order_id = $orderId LIMIT 1
                            
                        ");
  $rowhead = mysqli_fetch_array($headercon);
  $CreateDocument_head = $rowhead['CreateDocument'];
  $OrderNo_head = $rowhead['order_no'];

  $theader .= '</br>';
  $theader .= '<table border="0" style="padding-bottom:20px">';
  $theader .= '<tr style="">';
  $theader .= '<th colspan="3" align="">';
  $theader .= 'Order No. : '.$OrderNo_head.'';
  $theader .= '</th>';      
  $theader .= '<th colspan="4" align="">';
  $theader .= 'Order Date : '.date("d-m-Y", strtotime($CreateDocument_head)).'';
  $theader .= '</th>';
  $theader .= '</tr>';
  $theader .= '<tr style="">';
  $theader .= '<th colspan="3" align="">';
  $theader .= 'Report by : '.$reportby.'';
  $theader .= '</th>';      
  $theader .= '<th colspan="4" align="">';
  $theader .= '';
  $theader .= '</th>';
  $theader .= '</tr>';
  $theader .= '</table>';
  $theader .= '</br>';

  $theader .= '<table border="1">';
  $theader .= '<tr style="">';
  $theader .= '<th colspan="1" align="center">';
  $theader .= 'No.';
  $theader .= '</th>';    
  $theader .= '<th colspan="4" align="center">';
  $theader .= 'Product Name';
  $theader .= '</th>';
  $theader .= '<th colspan="2" align="center">';
  $theader .= 'Price';
  $theader .= '</th>';   
  $theader .= '<th colspan="3" align="center">';
  $theader .= 'Qty';
  $theader .= '</th>';
  $theader .= '<th colspan="2" align="center">';
  $theader .= 'Total';
  $theader .= '</th>';
  $theader .= '</tr>';

  $i = 1;
  $Total = 0 ;
  $Discount = 0;
  $Promotion = 0;
  $GrandTotal = 0;

  while($row = mysqli_fetch_array($dbcon))
  {
      $CreateDocument = $row['CreateDocument'];
      $item_qty = number_format((float)$row['item_qty'], 2, '.', '');
      $item_total = number_format((float)$row['item_total'], 3, '.', '');
      $product_name = $row['product_name'];
      $product_price = number_format((float)$row['product_price'], 3, '.', '');

      $theader .= '<tr style="">';
      $theader .= '<td colspan="1" align="center">';
      $theader .= ''.$i.'';
      $theader .= '</td>';
      $theader .= '<td colspan="4">';
      $theader .= ' '.$product_name.'';
      $theader .= '</td>';
      $theader .= '<td colspan="2"  align="right">';
      $theader .= ''.$product_price.' <span> </span>';
      $theader .= '</td>';
      $theader .= '<td colspan="3" align="right">';
      $theader .= ''.$item_qty.' <span> </span>';
      $theader .= '</td>';
      $theader .= '<td colspan="2"  align="right">';
      $theader .= ''.$item_total.' <span> </span>';
      $theader .= '</td>';
      $theader .= '</tr>';

      $Promotion=0;
      $Total += $item_total;
      $i++;
      
  }

  // $realrow = mysqli_num_rows($dbcon);
  // $faderow = 30;
  // $genrow = $faderow - $realrow;

  // for ($n=0; $n < $genrow ; $n++) { 
  //   $theader .= '<tr style="">';
  //   $theader .= '<td colspan="1" align="center">';
  //   $theader .= '';
  //   $theader .= '</td>';
  //   $theader .= '<td colspan="4">';
  //   $theader .= '';
  //   $theader .= '</td>';
  //   $theader .= '<td colspan="2"  align="right">';
  //   $theader .= '';
  //   $theader .= '</td>';
  //   $theader .= '<td colspan="3">';
  //   $theader .= '';
  //   $theader .= '</td>';
  //   $theader .= '<td colspan="2"  align="right">';
  //   $theader .= '';
  //   $theader .= '</td>';
  //   $theader .= '</tr>';
  // }

  $Discount = ($Total* $Promotion)/100;
  $GrandTotal = $Total - $Discount;
  $theader .= '</table>';
  $theader .= '<p ></p>';
  $theader .= '<table border="0" style="padding-top:10px">';
  $theader .= '<tr style="">';
  $theader .= '<th colspan="7" align="center">';
  $theader .= '';
  $theader .= '</th>';      
  $theader .= '<th colspan="2" align="">';
  $theader .= '<b>Total</b>';
  $theader .= '</th>';
  $theader .= '<th colspan="3" align="right">';
  $theader .= ''.number_format((float)$Total, 3, '.', '').' Baht';
  $theader .= '</th>';
  $theader .= '</tr>';

  $theader .= '<tr style="">';
  $theader .= '<th colspan="7" align="center">';
  $theader .= '';
  $theader .= '</th>';      
  $theader .= '<th colspan="2" align="">';
  $theader .= '<b>Discount</b>';
  $theader .= '</th>';
  $theader .= '<th colspan="3" align="right">';
  $theader .= ''.number_format((float)$Discount, 2, '.', '').' Baht';
  $theader .= '</th>';
  $theader .= '</tr>';

  $theader .= '<tr style="">';
  $theader .= '<th colspan="7" align="center">';
  $theader .= '';
  $theader .= '</th>';      
  $theader .= '<th colspan="2" align="">';
  $theader .= '<b>GrandTotal</b>';
  $theader .= '</th>';
  $theader .= '<th colspan="3" align="right">';
  $theader .= ''.number_format((float)$GrandTotal , 3, '.', '').' Baht';
  $theader .= '</th>';
  $theader .= '</tr>';

  $theader .= '</table>';

  $tfooter = '';
  // Print text using writeHTMLCell()
  $pdf->writeHTML($theader.$tfooter, true, false, false, false, '');
  // ---------------------------------------------------------
  //Close and output PDF document
  $date = date("d/m/Y");
  list($d_o,$m_o,$Y_o) = split('/',$date);
  $Y_o = $Y_o + 543;
  $pdf->Output(''.$d_o.'-'.$m_o.'-'.$Y_o.'.pdf', 'I');
  mysqli_close($con);

?>
