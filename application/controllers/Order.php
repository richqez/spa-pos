<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	function __construct() {
        parent::__construct();
        set_secure_zone();
    }

	public function index($output = null)
	{
		$this->load->view('section_top');
		$this->load->view('order_list',[
				'orders' => $this->db->get('orderdocument')->result()
				]);
		$this->load->view('section_bottom');
	}

    public function create($output = null)
	{
		$this->load->view('master',[
			'output'=>$this->load->view('create_order','',true),
			'vm'=> 'order.js'
		]);
	}

	public function forceComplate($value,$v)
	{
		$this->db->where('employee_id',$value);
		$this->db->update('employee', ["IsActive"=>'1']);
		redirect(base_url()  .  'order/view/'  . $v ,'refresh');

	}

	public function view($order_id){
		
		$this->db->select('*');
		$this->db->from('orderdocument');
		$this->db->join('orderdocumentitem', 'orderdocumentitem.item_orderid = orderdocument.order_id');
		$this->db->join('product', 'product.product_id = orderdocumentitem.item_productid');
		$this->db->join('producttype', 'product.product_producttypeid = producttype.producttype_id');
		$this->db->join('employee', 'employee.employee_id = orderdocumentitem.item_employeeid','left');
		$this->db->where('orderdocument.order_id',$order_id);

		$this->load->view('section_top');
		$orders = $this->db->get()->result() ;

		$promo = null ;

		if (isset($orders[0]->order_promotecode)) {
			
			$promo = $this->db->get_where('promotion',['promo_code'=>$orders[0]->order_promotecode])->row();

		}


		$this->load->view('view_order',[
				'items' => $orders,
				'order' => $orders[0],
				'promo' => $promo ? $promo : null
				]);
		$this->load->view('section_bottom');
	
	}

	public function updateOrderState($id,$state)
	{
		$this->db->where('order_id', $id);
		$this->db->update('orderdocument', ["order_state"=>$state]);

		if ($state == 2 && $state == 3) {

				$query = $this->db->get_where('orderdocumentitem', array('item_orderid' => $id))->result();

				foreach ($query  as $value) {
					if (!is_null($value->item_employeeid)) {
						$this->db->where('employee_id', $value->item_employeeid);
						$this->db->update('employee', ["IsActive"=>'1']);
					}
				}				
		}

		redirect(base_url()  .  'order/view/'  . $id ,'refresh');
	}

	public function saveOrder()
	{
		date_default_timezone_set('UTC');

		$postData = json_decode(file_get_contents('php://input'));

		//var_dump($postData);
		
		$c = $this->db->get('orderdocument')->num_rows();
	
		$this->db->select('*');
		$this->db->from('promotion');
		$this->db->where('criteria <=', $postData->total);
		$this->db->where('CAST('  . "'". date('Y-m-d H:i:s' ) . "'". ' AS DATE ) BETWEEN '  . 'promo_from AND promo_to' );     
		$this->db->where('promo_quantity >', '0');

		
		$promo = $this->db->get()->row();


		//var_dump($promo);

		$newOrder = [
			"order_no" => 'PO-' .  str_pad($c , 8 , '0' ),
			"order_employeeid" => "" ,
			"order_total" => $postData->total ,
			"order_discount" => 0 ,
			"order_promotecode" => $promo ? $promo->promo_code : "",
			"order_state" => $postData->isReserve == 'true' ? 5 : 1
		];


		$this->db->insert('orderdocument', $newOrder); 
		
		$orderId = $this->db->insert_id();


		foreach ($postData->orderItem as  $z) {



			$newOrderItem = [
				"item_orderid" => $orderId ,
				"item_productid" => $z->productId,
				"item_qty" =>$z->qty,
				"item_total" => $z->total,
				"item_employeeid" => $z->employeeId,
				"item_from" => !is_null($z->from ) ? date( "Y-m-d H:i:s",strtotime($z->from.  " +7 hours")) : null , 
				"item_to" => !is_null($z->to) ? date( "Y-m-d H:i:s",strtotime($z->to .  " +7 hours")) : null,
				"item_date" => !is_null($z->zDate) ? date("Y-m-d",strtotime($z->zDate)) : null 

			];
			
			

			$this->db->insert('orderdocumentitem', $newOrderItem);
		
			if (!is_null($z->employeeId)) {
				$this->db->where('employee_id', $z->employeeId);
				$this->db->update('employee', ["IsActive"=>'0']);
			}

		}


		if ($promo) {
			$this->db->where('promo_id', $promo->promo_id);
			$this->db->update('promotion', ["promo_quantity"=> $promo->promo_quantity  - 1 ]);
		}
		 

		echo $orderId;

	}


	public function getProduct()
	{
		$this->db->select('*');
		$this->db->from('product');
		$this->db->join('producttype', 'product.product_producttypeid = producttype.producttype_id');
		echo json_encode($this->db->get()->result());
	}

	public function getEmployee($date)
	{

		$r = [];

		$emps = $this->db->get('employee')->result();

		foreach ($emps as $value ) {

			$this->db->select('*');
			$this->db->from('orderdocumentitem');
			$this->db->join('orderdocument', 'orderdocumentitem.item_orderid = orderdocument.order_id');
			$this->db->where([
					"item_employeeid"=>$value->employee_id,
					"item_date" =>  date("Y-m-d",strtotime($date)),
					
				]);

			$items = $this->db->get()->result();

			$canSelect = true ;
			foreach ($items as $zx) {
				if ($zx->order_state == 1) {
					$canSelect = false ;
				}
			}
			$value->IsActive = $canSelect ?  1 : 0;

			





			$value->order_r = $items ;

			
		};





		echo json_encode($emps);
	}

	

}
