<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {


	public function index($output = null)
	{
		if (!isLogin()) {
			$this->load->view('signin',$output);
		}else{
			redirect(base_url() . 'order/Index','refresh');
		}

	}


	public function login()
	{
		
 		$query = $this->db->get_where('user', ["user_name"=>$this->input->post('username')]);
		$user = $query->first_row();
		if ($user) {
			if ($user->user_password == $this->input->post('password')) {
				$this->session->set_userdata('current_user',$user); 
				redirect(base_url() . 'order/Index','refresh');
			}
		}else {
			$this->session->set_flashdata('msg', 'UserName Or Password Invalid');
			redirect(base_url() . 'Home/Index');
		}
	}


	public function logout()
	{
		destroy_current_user_session();
		redirect(base_url(),'refresh');
	}




}
