<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller {


	public function index($output = null)
	{
		$crud = new grocery_CRUD();
		
		$crud->set_table('promotion')
			->display_as('promo_description','Description')
			->display_as('promo_code','Code')
			->required_fields('promo_description','promo_code');
		
		$crud->add_action('SendToLine', '', '','ui-icon-image',array($this,'just_a_test'));
		$output = $crud->render();
		$this->load->view('master',$output);
	}

	function just_a_test($primary_key , $row)
	{
		return base_url() .'promo/line/'.$row->promo_id;
	}

	function line($id){

		$token = 'QWwNiZXggu0CmcCCYxhnW9WQ1x4Ar9C2vILUOnOrde5';
		$query = $this->db->get_where('promotion', array('promo_id' => $id));
		$data  = $query->row();
		$this->send_line_notify($data->promo_description,$token );
		redirect(base_url()  .  'promo','refresh');
	}


	function send_line_notify($message, $token)
	{
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, "https://notify-api.line.me/api/notify");
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt( $ch, CURLOPT_POST, 1);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, "message=$message");
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
		$headers = array( "Content-type: application/x-www-form-urlencoded", "Authorization: Bearer $token", );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec( $ch );
		curl_close( $ch );

		return $result;
	}


}