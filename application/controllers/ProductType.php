<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductType extends CI_Controller {


	function __construct() {
        parent::__construct();
        set_secure_zone();
    }
	
	public function index($output = null)
	{
		$crud = new grocery_CRUD();
		
		$crud->set_table('producttype')
			->display_as('producttype_name','Product Type')
			->required_fields('producttype_name');
		$crud->unset_add_fields('createdDate','producttype_id');
		$crud->unset_edit_fields('createdDate','producttype_id');

		$output = $crud->render();
		$this->load->view('master',$output);
	}	
}


