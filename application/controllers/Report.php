<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {


	public function Dairy($output = null)
	{



        $this->load->view('section_top');
        $this->load->view('Report1');
        $this->load->view('section_bottom');
	}

	public function Product($output = null)
	{

		$this->db->select('*');
		$this->db->from('product');

        $this->load->view('section_top');
        $this->load->view('ProductSummaryReport',['product' => $this->db->get()->result()]);
        $this->load->view('section_bottom');
	}

	public function ServiceReport($output = null)
	{

		$this->db->select('*');
		$this->db->from('producttype');

        $this->load->view('section_top');
        $this->load->view('ServiceSummaryReport',['product' => $this->db->get()->result()]);
        $this->load->view('section_bottom');
	}


	public function getProduct()
	{
	}

}
