<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	function __construct() {
        parent::__construct();
        set_secure_zone();
    }

	public function index($output = null)
	{
		$crud = new grocery_CRUD();
		//$crud->set_theme('bootstrap');
		$crud->set_table('product');
        $crud->display_as('product_producttypeid','Product Type');
        $crud->set_subject('Product');
		$crud->unset_add_fields('createdDate','product_id');
        $crud->unset_edit_fields('createdDate','product_id');
        $crud->set_rules('product_price','product Price','numeric');
        $crud->set_relation('product_producttypeid','producttype','producttype_name');
		$output = $crud->render();
		$this->load->view('master',$output);
	}
}


