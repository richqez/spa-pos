<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {


	public function index($output = null)
	{
		$crud = new grocery_CRUD();
		
		$crud->set_table('employee')
			->display_as('employee_fname','First Name')
			->display_as('employee_lname','Last Name')
			->display_as('employee_address','Address')
			->display_as('employee_tel','Tel')
			->display_as('employee_age','Age')
			->required_fields('employee_fname','employee_lname','employee_address','employee_tel','employee_age');

		$output = $crud->render();
		$this->load->view('master',$output);
	}



}
