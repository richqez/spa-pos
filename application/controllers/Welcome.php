<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
        parent::__construct();
        set_secure_zone();
    }

	public function index($output = null)
	{
		$crud = new grocery_CRUD();
	//	$crud->set_theme('bootstrap');
		$crud->set_table('employee');
		$output = $crud->render();
		$this->load->view('welcome_message',$output);
	}

	public function add($output = null){
		$crud = new grocery_CRUD();
		$crud->set_table('employee');
		$crud->unset_columns('createdDate','employee_id');
		$output = $crud->render();
		$this->load->view('welcome_message',$output);
	}

}
