<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
        parent::__construct();
        set_secure_zone();
    }

	public function index($output = null)
	{
		$crud = new grocery_CRUD();
		
		$crud->set_table('user');
		$crud->unset_add_fields('user_id','createdDate');
		$output = $crud->render();
		$this->load->view('master',$output);
	}
}


