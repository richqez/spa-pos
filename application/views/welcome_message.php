<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?php 
	foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
	
	<?php endforeach; ?>
	<?php foreach($js_files as $file): ?>
	
		<script src="<?php echo $file; ?>"></script>
	<?php endforeach; ?>
</head>
<body>
	<?php echo $output; ?>
</body>
</html>