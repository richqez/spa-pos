  

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase">Product Summary Report</span>
            </div>
        </div>
        <div class="portlet-body">
            <form role="form">
            
                <div class="form-body">
                   <!--  <div class="form-group">
                        <label>Service Name</label>
                         <select id="producttype" class="form-control" >
                                 <option value="">All Service</option>
                                <?php foreach ($producttype as $v): ?>
                                    <option value="<?php echo $v->producttype_id ?>"><?php echo $v->producttype_name ?></option>
                                <?php endforeach ?>
                        </select>   
                           
                    </div> -->
                    <div class="form-group">
                        <label>Product Name</label>
                         <select id="product" class="form-control" >
                                 <option value="">All Product</option>
                                <?php foreach ($product as $v): ?>
                                    <option value="<?php echo $v->product_id ?>"><?php echo $v->product_name ?></option>
                                <?php endforeach ?>
                        </select>   
                           
                    </div>
                    <div class="form-group">
                        <label>From Date</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-envelope"></i>
                            </span>
                            <input id="from" type="date" class="form-control" > </div>
                    </div>
                    <div class="form-group">
                        <label>To Date</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-envelope"></i>
                            </span>
                            <input id="to" type="date" class="form-control" > </div>
                    </div>

                <div class="form-actions">
                    <button id="view" type="button" class="btn blue">Submit</button>
                </div>
            </form>
        </div>
    </div>


<?php $user = getCurrentUser() ; ?>

<script>

    $('#view').click(()=>{
        let  from = $('#from').val();
        let product = $('#product').val();
        let to = $('#to').val();
        //let producttype = $('$producttype').val();

        if (from && to) {
             window.open(webUrl + 'report/ProductSummaryReport.php?FromDate='+from +'&ToDate=' + to + '&ProductId='+ product +"&reportby=<?php echo $user->user_name  ?>" );
        }
    });
    

</script>
