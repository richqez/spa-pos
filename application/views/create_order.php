<div class="portlet light portlet-fit portlet-datatable bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings font-green"></i>
            <span class="caption-subject font-green sbold uppercase">Create Order </span>
        </div>
          <div class="actions">
              <a href="javascript:;" class="btn btn-lg blue"  data-bind="click:createOrder"> 
                <i class="fa fa-file-o"></i> Create Order 
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row static-info">
            <div class="col-md-5 name"> Order #: </div>
            <div class="col-md-7 value"> 
                -
            </div>
        </div>
        <div class="row static-info">
            <div class="col-md-5 name"> Order Date &amp; Time: </div>
            <div class="col-md-7 value"> <span data-bind="text:orderDate"></span> </div>
        </div>
        <div class="row static-info">
            <div class="col-md-5 name"> Order Status: </div>
            <div class="col-md-7 value">
                <span class="label label-primary"> Draft </span>
            </div>
        </div>
        <div class="row static-info">
            <div class="col-md-5 name"> Grand Total: </div>
            <div class="col-md-7 value"> <span data-bind="text:total"></span> </div>
        </div>


        <div class="row static-info">
            <div class="col-md-5 name"> Reservation </div>
            <div class="col-md-7 value">
                <input type="checkbox" data-bind="checked: isReserve" />
            </div>
        </div>

         <div class="row static-info" data-bind="visible:isReserve">
            <div class="col-md-5 name"> ReservationDate </div>
            <div class="col-md-7 value">
                <input data-bind="value:dateX,enable:isReserve" class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="">
            </div>
        </div>




        <div class="actions">
            <a href="javascript:;" class="btn btn-icon-only green" data-bind="click:addItem" >
                <i class="fa fa-plus"></i>
            </a>
  
            
        </div>
        <div class="table-container">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>Product Type</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                    <tbody data-bind="foreach:orderItem">
                        <tr>
                            <td><span data-bind="text:($index() + 1)"></span></td>
                            <td>
                                 <select class="form-control" 
                                 data-bind="options:$parent.productItem,
                                             optionsText:'product_name',
                                             value:productId,
                                             optionsCaption:'SelectProduct'">
                                </select>

                                <div class="portlet blue-hoki box" data-bind="with:employee">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Massage staff </div>
                                        <div class="actions">
                                            <a href="javascript:;" class="btn btn-default btn-sm"
                                                data-bind="click:function(){
                                            
                                                    $parent.OrderVM.openModal($parent)
                                                }"
                                            >
                                                <i class="fa fa-pencil"></i> Edit </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> First Name: </div>
                                            <div class="col-md-7 value" data-bind="text:e().employee_fname"> Jhon Doe </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Last Name: </div>
                                            <div class="col-md-7 value" data-bind="text:e().employee_lname"> jhon@doe.com </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Age: </div>
                                            <div class="col-md-7 value" data-bind="text:e().employee_age"> New York </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Date: </div>
                                            <div class="col-md-7 value" >
                                                <span data-bind="text:dateX"></span>
                                             </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> From: </div>
                                            <div class="col-md-7 value" >
                                                <input data-bind="value:from" type="text" class="form-control timepicker timepicker-24">
                                          
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> To: </div>
                                            <div class="col-md-7 value" >
                                             <input data-bind="value:to" type="text"  class="form-control timepicker timepicker-24">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td><span data-bind="text:type()"></span></td>
                            <td><span data-bind="text:price()"></span></td>
                            <td><input class="form-control" type="number" data-bind="value:qty,enable:!employee()" min="1" /></td>
                            <td><span data-bind="text:total()"></span></td>
                            <td>
                                <button class="btn btn-sm red btn-outline filter-cancel" data-bind="click:removeItem">
                                    <i class="fa fa-times"></i> 
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>   
        </div>     
    </div>
</div>


 <div class="modal fade" id="full" tabindex="-1" role="dialog" aria-hidden="true" data-bind="with:modal"> 
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Select Staff Massage</h4>
            </div>
            <div class="modal-body">

                <table class="table">
                    <tbody data-bind="foreach:employeeItem">
                        <tr>
                            <td>
                                <h1><span data-bind="text:employee_fname + ' ' + employee_lname"></span></h1>
                                <ul data-bind="foreach:order_r">
                                    <li>
                                        <a data-bind="attr:{ href:webUrl +'order/view/' +order_id  }" target="_blank">
                                        <span data-bind="text:order_no +' ( ' +item_from + ' - ' + item_to + ' )'"></span></a>
                                        
                                        <!-- ko if: order_state == 1 -->
                                            <span class="label label-warning"> Waiting For Payment </span>
                                        <!-- /ko -->


                                        <!-- ko if: order_state == 5 -->
                                            <span class="label label-success"> Reservation </span>
                                        <!-- /ko -->

                                    </li>
                                </ul>
                            
                            </td>
                            <td>
                                <a href="javascript:;" class="btn grey-cascade" data-bind="click:function(){ $parent.select($data)},visible:IsActive == 1 "> Select It,.
                                    <i class="fa fa-link"></i>
                                </a></td>
                        </tr>
                    </tbody>
                </table>
                <h2 data-bind="text:totalItem() + ' Items '"></h2>
                <div class="btn-group" >
                    <button type="button" class="btn btn-default" data-bind="click:perv"> <--- </button>

                    <!-- ko foreach:pageBtns -->
                    <button type="button" class="btn btn-default" data-bind="text:$data,click:function(){
                        $parent.setCurrentPage($data)

                    }">1</button>
                    <!-- /ko -->


                    <button type="button" class="btn btn-default" data-bind="click:fwr"> ---> </button>

                </div>
   

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>