<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	
    <?php require('css.php') ?>
    <?php require('script.php') ?>
		

</head>
<body  class="page-md">
    <?php require('header.php') ?>


		 <div class="container-fluid">
            <div class="page-content page-content-popup">
                <div class="page-content-fixed-header">
                    <!-- BEGIN BREADCRUMBS -->
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="#">Apps</a>
                        </li>
                    </ul>
                    <!-- END BREADCRUMBS -->
                    <div class="content-header-menu">
                        
                       
                        <!-- BEGIN MENU TOGGLER -->
                        <button type="button" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="toggle-icon">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </span>
                        </button>
                        <!-- END MENU TOGGLER -->
                    </div>
                </div>
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                            <li class="nav-item start ">
                                <li class="nav-item start ">
                                    <a href="<?php echo base_url(); ?>Promo" class="nav-link ">
                                        <i class="icon-present"></i>
                                        <span class="title">Promotions</span>
                                    </a>
                                </li>
                                <li class="nav-item start ">
                                    <a href="<?php echo base_url(); ?>Order" class="nav-link ">
                                        <i class="icon-bar-chart"></i>
                                        <span class="title">Order Management</span>
                                    </a>
                                </li>
                            </li>
                            <?php $user = getCurrentUser() ; ?>
                            <?php if ( $user->user_role == '100' ): ?>
                                 <li class="heading">
                                    <h3 class="uppercase">Master Data</h3>
                                </li>
                                <li class="nav-item  ">
                                     <li class="nav-item  ">
                                        <a href="<?php echo base_url(); ?>Product" class="nav-link ">
                                            <i class="icon-layers"></i>
                                            <span class="title">Products</span>
                                        </a>
                                    </li>
                                     <li class="nav-item  ">
                                        <a href="<?php echo base_url(); ?>ProductType" class="nav-link ">
                                            <i class="icon-layers"></i>
                                            <span class="title">Products Type</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  start">
                                        <a href="<?php echo base_url(); ?>Employee" class="nav-link ">
                                           <i class="icon-user"></i>
                                            <span class="title">Employee</span>
                                        </a>
                                    </li>
                                     <li class="nav-item  ">
                                        <a href="<?php echo base_url(); ?>User" class="nav-link ">
                                            <i class="icon-user"></i>
                                            <span class="title">User</span>
                                        </a>
                                    </li>                                
                                </li>
                            <?php endif ?>
                             <li class="heading">
                                <h3 class="uppercase">Reports</h3>
                            </li>
                             <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-graph"></i>
                                    <span class="title">Reports</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                         <a href="<?php echo base_url(); ?>Report/Dairy" class="nav-link ">
                                            <span class="title">Summary Order</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                         <a href="<?php echo base_url(); ?>Report/ServiceReport" class="nav-link ">
                                            <span class="title">Service Summary </span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                         <a href="<?php echo base_url(); ?>Report/Product" class="nav-link ">
                                            <span class="title">Product Summary </span>
                                        </a>
                                    </li>
                                     <!-- <li class="nav-item  ">
                                        <a href="ui_colors.html" class="nav-link ">
                                            <span class="title">Color Library</span>
                                        </a> 
                                    </li> -->
                                </ul>
                            </li>
                        </ul>
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <div class="page-fixed-main-content">
                    <!-- BEGIN PAGE BASE CONTENT -->