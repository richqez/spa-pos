  

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase"> Order List Table</span>
            </div>
                 <div class="actions">
                    <a class="btn blue-hoki" href="<?php echo base_url() ?>/order/create" >Create Order</a>
                </div>

            
              
        </div>
        <div class="portlet-body">
              <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
    <thead>
        <tr>
            <th> Orde No </th>
            <th> Status </th>
            <th> Total </th>
            <th> OrderDate </th>
            <th> Actions </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($orders as $item):?>
            <tr>
                <td>
                    <?php echo $item->order_no ?>
                </td>
                <td>
                    <?php if ($item->order_state == 1) { ?>
                        <span class="label label-warning"> Waiting For Payment </span>
                    <?php } elseif ($item->order_state  == 2) {?>
                        <span class="label label-success"> Complated </span>
                    <?php } elseif ($item->order_state  == 5) {?>
                         <span class="label label-success"> Reservation </span>
                    <?php } else{ ?>
                        <span class="label label-danger"> Canceled </span>
                    <?php } ?>
                </td>
                <td>
                    <?php echo $item->order_total ?>
                </td>
                <td>
                    <?php echo $item->createdDate ?>
                </td>
                <td>
                    <a class="btn blue-hoki" href="<?php echo base_url() ?>/order/view/<?php echo $item->order_id ?>" >View</a>
                </td>
            </tr>
        <?php endforeach;?>   
            
        
    </tbody>
</table>

        </div>
    </div>


<script>
     $('#sample_1').DataTable();
</script>