

             
<div class="portlet light portlet-fit portlet-datatable bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings font-green"></i>
            <span class="caption-subject font-green sbold uppercase">View Order </span>
        </div>
        <div class="actions">
         <?php if ($order->order_state  == 1) { ?>
             
              <a href="<?php echo base_url() ?>/order/updateOrderState/<?php echo $order->order_id ?>/2" class="btn btn-lg blue"  > 
                <i class="fa fa-file-o"></i> Complate Order
              </a>
              <a href="<?php echo base_url() ?>/order/updateOrderState/<?php echo $order->order_id ?>/3" class="btn btn-lg blue"  > 
                <i class="fa fa-file-o"></i> Cancel Order
              </a>
         <?php } ?>

         <?php if ($order->order_state  == 5) { ?>
             
              <a href="<?php echo base_url() ?>/order/updateOrderState/<?php echo $order->order_id ?>/1" class="btn btn-lg blue"  > 
                <i class="fa fa-file-o"></i> Confirm Order
              </a>
         <?php } ?>

         <?php $user = getCurrentUser() ; ?>
         <?php if ($order->order_state  == 2) { ?>
            <a href="<?php echo base_url() ?>report/PrintForm.php?orderId=<?php echo $order->order_id ?>&reportby=<?php echo $user->user_name  ?>" class="btn btn-lg blue"  > 
                <i class="fa fa-file-o"></i> GetInvoice
              </a>
         <?php } ?>
         </div>
    </div>
    <div class="portlet-body">
        <div class="row static-info">
            <div class="col-md-5 name"> Order #: </div>
            <div class="col-md-7 value"> 
                <?php echo $order->order_no ?>
            </div>
        </div>
        <div class="row static-info">
            <div class="col-md-5 name"> Order Date &amp; Time: </div>
            <div class="col-md-7 value"> <?php echo $order->createdDate ?></div>
        </div>
        <div class="row static-info">
            <div class="col-md-5 name"> Order Status: </div>
            <div class="col-md-7 value">

                <?php if ($order->order_state == 1) { ?>
                   <span class="label label-warning"> Waiting For Payment </span>
                <?php } elseif ($order->order_state  == 2) {?>
                   <span class="label label-success"> Complated </span>
                 <?php } elseif ($order->order_state  == 5) {?>
                   <span class="label label-success"> Reservation </span>
                <?php } else{ ?>
                     <span class="label label-danger"> Canceled </span>
                <?php } ?>



                
            </div>
        </div>
        <div class="row static-info">
            <div class="col-md-5 name"> Grand Total: </div>
            <div class="col-md-7 value"> <?php echo $order->order_total ?> </div>
        </div>
         <div class="row static-info">
            <div class="col-md-5 name"> Promotion: </div>
            <div class="col-md-7 value"> 
           <?php

                    echo($promo->promo_description);
                
            ?>
            </div>
        </div>
        </div>
        <div class="table-container">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Product Name</th>
                        <th>Product Type</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
    
                    <?php foreach ($items as $item):?>
                        <tr>
                            <td><?php echo $item->product_name ?></td>
                            <td><?php echo $item->producttype_name ?></td>
                            <td><?php echo $item->product_price ?></td>
                            <td><?php echo $item->item_qty ?></td>
                            <td><?php echo $item->item_total ?></td>
                            <td>
                                <?php if(isset($item->IsActive) && $item->IsActive == 0 ){?>
                                    <a href="<?php echo base_url() ?>/order/forceComplate/<?php echo $item->employee_id ?>/<?php echo $order->order_id ?>" class="btn btn-sm blue"  > 
                                        <i class="fa fa-file-o"></i> ForceComplate
                                    </a>
                                <?php }?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                </table>



            </div>   
        </div>     
    </div>
</div>