<!-- END QUICK SIDEBAR -->
	<!--[if lt IE 9]>
		<script src="<?php echo base_url() ?>/assets/global/plugins/respond.min.js"></script>
		<script src="<?php echo base_url() ?>/assets/global/plugins/excanvas.min.js"></script> 
	<![endif]-->
		<!-- BEGIN CORE PLUGINS -->
		<!-- BEGIN CORE PLUGINS -->
		<?php if(isset($js_files )) { ?>

            <?php foreach($js_files as $file): ?>
                <script src="<?php echo $file; ?>"></script>
            <?php endforeach; ?>


        <?php }else{ ?>
			<script src="<?php echo base_url() ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
		<?php } ?>
        
		<script src="<?php echo base_url() ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		
		
		
		
		<!-- END CORE PLUGINS -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
	
		<!-- END PAGE LEVEL PLUGINS -->

			<script src="<?php echo base_url() ?>/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>/assets/global/plugins/moment.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>/assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>

		<!-- BEGIN THEME GLOBAL SCRIPTS -->
		<script src="<?php echo base_url() ?>/assets/global/scripts/app.min.js" type="text/javascript"></script>
		<!-- END THEME GLOBAL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?php echo base_url() ?>/assets/apps/scripts/inbox.min.js" type="text/javascript"></script>

		<script src="<?php echo base_url() ?>/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>

		
		<!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
		<script src="<?php echo base_url() ?>/assets/layouts/layout6/scripts/layout.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
	 	<!-- END THEME LAYOUT SCRIPTS -->
	   <script src="<?php echo base_url() ?>/assets/global/scripts/knockout-3.4.2.js" type="text/javascript"></script>
	   <script src="<?php echo base_url() ?>/assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
	   <script src="<?php echo base_url() ?>/assets/global/plugins/sweetalert.min.js" type="text/javascript"></script>


	