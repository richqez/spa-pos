<?php

 function isLogin(){

    $CI =& get_instance();

    if ($CI->session->userdata('current_user') != NULL) {
      return TRUE;
    }
    else{
      return FALSE;
    }


  }

  function set_secure_zone(){
    if (!isLogin()) {
      redirect(base_url(),'refresh');
    }
  }

  function getCurrentUser(){
    $CI =& get_instance();
    return $CI->session->userdata('current_user');
  }

  function destroy_current_user_session(){
    $CI =& get_instance();
    $CI->session->sess_destroy();
  }


?>