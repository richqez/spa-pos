


class OrderVM {

    
    constructor(){

        this.dateX = ko.observable(null);
        
        this.isReserve = ko.observable(null);
        this.orderDate = ko.observable(new Date());
        this.orderItem = ko.observableArray([]);
        this.productItem = ko.observableArray([]);
        this.employeeItem = ko.observableArray([]);

        this.isReserve.subscribe(n=>{
            if(!n){
                let d = new Date(),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                let z =  [month,day,year].join('/');
                this.dateX(z);
            }

        });


        this.dateX.subscribe(x=>{
            if(x){
                let p = x.replace('/','-').replace('/','-');
                let s = p.split('-');
                let z = s[2] + '-' + s[0] + '-' + s[1]; 

                $.getJSON(webUrl + 'order/getEmployee/'+z,v =>{
                    this.employeeItem(v);
                });
            }
        });



        this.isReserve(false);

        this.modal = ko.observable(new EmployeeModal(this));

        this.total = ko.computed(()=>{
            return this.orderItem().reduce((x, y) => x + y.total(), 0);
        })

        this.selectedProduct = ko.computed(()=>{
            return this.orderItem()
            .map(x=> {
                return x.productId()? x.productId().product_id: null
            })
            .filter(k=> k);
        })


        $.getJSON(webUrl + 'order/getProduct' ,v =>{
            this.productItem(v);
        });

        


        this.orderItem.push(new OrderItemVM(this));

     

    }

    createOrder(){
        


        try {

            if(this.orderItem().length < 1){
                throw "error";
            }

            let orderItem = [];

            this.orderItem()
                .forEach(item=>{
                    orderItem.push(item.getOrderItem());
                });

          

            let order = {
                isReserve: this.isReserve(),
                total: this.total(),
                orderItem: orderItem
            };


            if(order){
                  $.ajax({
                    type: 'POST',
                    url: webUrl + 'order/saveOrder',
                    data: JSON.stringify(order),
                    contentType: "application/json",
                    dataType: 'json'
                })
                .done(r=>{
                    window.location.href =  webUrl + "order/view/" + r ; 
                })
                .fail(error=>{

                });


              
            }

        } catch (error) {
             sweetAlert("Oops...", "Please Check OrderItem Again", "error");
        }


    }

    addItem(){
         this.orderItem.push(new OrderItemVM(this));
    }

    removeOrderItem(item){
        this.orderItem.remove(item);
    }

    openModal(item){
      $('#full').modal('show');
      this.modal().setSelectedItem(item);
    }

    closeModal(){
        $('#full').modal('hide');
    }

}


class EmployeeRowVM {

    

    constructor(o,itemVM){
        this.e = ko.observable(o);
        this.ItemVM = itemVM;
        this.from = ko.observable(null);
        this.to = ko.observable(null);
        this.dateX = ko.computed(()=>{
            return itemVM.OrderVM.dateX();
        });
      }



}

class OrderItemVM{

    constructor(orderVM){
        this.OrderVM = orderVM;

        this.productName = ko.observable(null);
        this.productId = ko.observable(null);
        this.price = ko.observable(null);
        this.qty = ko.observable(null);
        this.type = ko.observable(null);
        this.employee = ko.observable(null);

        this.from = ko.observable(null);
        this.to = ko.observable(null);
        this.xDate = ko.observable(null);
        
        this.qty2 = ko.computed(()=>{
            if(this.employee() && this.employee().from() && this.employee().to() ){

                let zx = this.employee().dateX();
                this.xDate(zx);

                let ff = this.employee().from().split(":");
                let f = new Date();
                f.setHours(ff[0]);
                f.setMinutes(ff[1]);

                let tt = this.employee().to().split(":");
                let t = new Date();
                t.setHours(tt[0]);
                t.setMinutes(tt[1]);

                let x = moment(t).diff(moment(f),'seconds');
                console.log( Math.floor(x / 60));


                this.to(t.toISOString());
                this.from(f.toISOString());

                this.qty(Math.floor(x / 60) / 60 );
                return x;
            }else{
                return 0
            }

        });


        this.total = ko.computed(() => {
             return this.price() * this.qty();
        });

        this.productId.subscribe(v=>{
            if(v){
                if(orderVM.selectedProduct().indexOf(v.product_id) > -1)
                {
                    sweetAlert("Oops...", "Product is in selected list..!!", "error");
                    this.removeItem();
                }else{
                    this.setProduct(v);
                }
            }else{
                this.unsetProduct();
            }
        });


    }

    unsetProduct(){
        this.productName(null);
        this.productId(null);
        this.price(null);
        this.qty(null);
        this.type(null);
    }

    setEmployee(o){
        this.employee(new EmployeeRowVM(o,this));
        $('.timepicker').timepicker({
            showSeconds: false,
            showMeridian: false,
        
    
        });
        $('.date-picker').datepicker();
        //this.employee().set(x);

    }
    setProduct(o){
        //this.productName(o.product_name);
        //this.productId(o.product_id);
        this.price(o.product_price);
        this.qty(1);
        this.type(o.producttype_name);
        
        if(o.producttype_id == 1){
            this.OrderVM.openModal(this)
        }
        //debugger
    }


    getOrderItem(){
        return {
            productId : this.productId().product_id ,
            employeeId : this.employee() ? this.employee().e().employee_id : null ,
            qty: this.qty() ,
            total: this.total() ,
            from: this.from(),
            to: this.to(),
            zDate: this.xDate()         
        }
    }




     removeItem() {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
            },
            ()=>{
            this.OrderVM.removeOrderItem(this);
            swal("Deleted!", "item has been deleted.", "success");
        });
     }


}


class EmployeeModal {
    constructor(o){

        this.selectRow = ko.observable(null);
        this.orderVM = o ;
        this.page = ko.observable(1);
        this.perPage = ko.observable(10);

        
        this.totalItem = ko.computed(()=>{
            return this.orderVM.employeeItem().length;
        });


        this.pageBtns = ko.computed(()=>{
            let items = [];
            for(let i = this.page()  ; i < (this.totalItem() / this.perPage()) && i > 0  && i <= this.page() +5  ; i++){
                items.push(i);
            }
            return items;
        })

        this.employeeItem = ko.computed(()=>{
            return o.employeeItem.slice(this.page() * this.perPage(),(this.page() + 1) *this.perPage());
        });
    }
    
    setSelectedItem(item){
        this.selectRow(item);
    }

    perv(){
        this.setCurrentPage(this.page() -this.perPage() > 0 ? this.page() - this.perPage() : 1 )
    }

    fwr(){
        this.setCurrentPage(this.totalItem() / this.perPage() -1 )
    }

    setCurrentPage(p){
        this.page(p);
    }

    select(o){
        this.selectRow().setEmployee(o);
        this.orderVM.closeModal()
        
       
        
        
    }
}


ko.applyBindings(new OrderVM());