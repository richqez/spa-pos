-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2017 at 06:44 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spa_pos`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `dairysummaryreport`
-- (See below for the actual view)
--
CREATE TABLE `dairysummaryreport` (
`CreateDocument` timestamp
,`Item_Qty` decimal(32,0)
,`Item_Total` decimal(32,0)
,`employee_id` int(11)
,`employee_fname` varchar(50)
,`employee_lname` varchar(50)
,`employee_age` int(11)
,`employee_address` text
,`employee_tel` varchar(10)
,`createdDate` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employee_id` int(11) NOT NULL,
  `employee_fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `employee_lname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `employee_age` int(11) NOT NULL,
  `employee_address` text COLLATE utf8_unicode_ci NOT NULL,
  `employee_tel` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IsActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `employee_fname`, `employee_lname`, `employee_age`, `employee_address`, `employee_tel`, `createdDate`, `IsActive`) VALUES
(1, 'Natale', 'Streetley', 34, '3 Sycamore Alley', '880-29-395', '2017-05-23 14:37:14', 1),
(2, 'Boote', 'Mayfield', 39, '919 Thackeray Junction', '494-30-632', '2017-05-23 14:37:14', 1),
(3, 'Trueman', 'Jobbins', 30, '44859 Scott Place', '975-06-504', '2017-05-23 14:37:14', 1),
(4, 'Padget', 'Conn', 21, '0 Sunnyside Terrace', '915-76-011', '2017-05-23 14:37:14', 1),
(5, 'Care', 'Lautie', 30, '4266 Bayside Circle', '355-56-716', '2017-05-23 14:37:14', 1),
(6, 'Norton', 'Seville', 34, '603 Thierer Alley', '993-08-550', '2017-05-29 15:55:57', 1),
(7, 'Page', 'Gerriet', 36, '0263 Swallow Street', '550-49-081', '2017-05-29 15:55:57', 1),
(8, 'Franz', 'Tremmil', 22, '88281 Ryan Road', '989-21-805', '2017-05-29 15:55:57', 1),
(9, 'Finlay', 'Marjoram', 30, '824 Dunning Place', '683-60-948', '2017-05-29 15:55:57', 1),
(10, 'Haze', 'Quemby', 24, '020 Novick Street', '432-81-946', '2017-05-29 15:55:57', 1),
(11, 'Farlee', 'McMeanma', 28, '44 Main Park', '465-86-069', '2017-05-29 15:56:06', 0),
(12, 'Oran', 'Mariault', 46, '67970 Johnson Crossing', '848-27-860', '2017-05-29 15:56:36', 0),
(13, 'Roscoe', 'Tremontana', 35, '4 Pine View Way', '315-85-628', '2017-05-29 16:21:53', 0),
(14, 'Wesley', 'Stearn', 30, '9 Maple Wood Center', '946-14-918', '2017-05-29 16:26:05', 0),
(15, 'Duncan', 'Cornilleau', 33, '0174 Bartelt Way', '453-51-034', '2017-05-29 16:24:19', 0),
(16, 'Patrice', 'Longmire', 42, '78 Comanche Way', '336-22-080', '2017-05-29 15:55:57', 1),
(17, 'Paten', 'Haddon', 50, '7816 Manitowish Court', '511-23-149', '2017-05-29 15:55:57', 1),
(18, 'Norry', 'Orgee', 49, '86 Coolidge Street', '483-23-691', '2017-05-23 14:37:14', 1),
(19, 'Conan', 'Faughnan', 34, '4170 Waywood Junction', '101-87-820', '2017-05-29 15:55:57', 1),
(20, 'Monro', 'Standell', 37, '08 Tomscot Drive', '150-74-019', '2017-05-23 14:37:14', 1),
(21, 'Daniel', 'Brumen', 45, '001 Laurel Place', '203-65-447', '2017-05-23 14:37:14', 1),
(22, 'Kelwin', 'Pendre', 32, '1 Hudson Park', '923-46-918', '2017-05-23 14:37:14', 1),
(23, 'Lorenzo', 'Peppard', 29, '64772 Raven Point', '327-97-707', '2017-05-29 15:55:57', 1),
(24, 'Rufe', 'Lindores', 24, '1108 Tomscot Alley', '454-18-928', '2017-05-29 15:55:57', 1),
(25, 'Olin', 'Rubanenko', 50, '7 Porter Center', '559-90-076', '2017-05-23 14:37:14', 1),
(26, 'Hamish', 'Trayton', 32, '54361 Leroy Court', '233-34-096', '2017-05-23 14:37:14', 1),
(27, 'Mitchel', 'Tudge', 47, '257 Fremont Point', '616-40-709', '2017-05-29 15:55:57', 1),
(28, 'Temple', 'Pawley', 27, '27 Manley Court', '660-05-311', '2017-05-29 15:55:57', 1),
(29, 'Morey', 'Peppard', 22, '99704 Mcbride Center', '828-28-724', '2017-05-23 14:37:15', 1),
(30, 'Kyle', 'Hamblin', 21, '4593 Longview Circle', '960-83-648', '2017-05-29 15:55:57', 1),
(31, 'Forrester', 'Dagon', 34, '7985 Fuller Way', '202-45-971', '2017-05-23 14:37:15', 1),
(32, 'Aylmer', 'Henrot', 24, '6 Waxwing Center', '951-38-236', '2017-05-29 15:55:57', 1),
(33, 'Alix', 'Archbutt', 47, '73667 Clarendon Trail', '434-19-193', '2017-05-29 15:55:57', 1),
(34, 'Lou', 'Dancey', 32, '95 Mifflin Street', '626-91-676', '2017-05-23 14:37:15', 1),
(35, 'Antone', 'Milne', 20, '2 Boyd Hill', '380-07-668', '2017-05-23 14:37:15', 1),
(36, 'Coop', 'Featherstone', 28, '93 Barnett Pass', '195-77-552', '2017-05-23 14:37:15', 1),
(37, 'Philip', 'Enrietto', 27, '3888 Bunting Way', '750-93-357', '2017-05-23 14:37:15', 1),
(38, 'Orrin', 'Sinnie', 34, '4005 Green Way', '249-33-243', '2017-05-23 14:37:15', 1),
(39, 'Standford', 'Josskowitz', 49, '40801 Luster Street', '764-10-640', '2017-05-23 14:37:15', 1),
(40, 'Andrew', 'Sidey', 50, '369 Talisman Circle', '475-69-647', '2017-05-23 14:37:15', 1),
(41, 'Billy', 'Rowbottam', 40, '9777 Kensington Lane', '106-59-218', '2017-05-23 14:37:15', 1),
(42, 'Rip', 'Inns', 21, '90 Heath Terrace', '864-91-673', '2017-05-23 14:37:15', 1),
(43, 'Andrey', 'Gedney', 33, '364 Luster Hill', '665-22-710', '2017-05-23 14:37:15', 1),
(44, 'Gregory', 'O\'dell', 45, '1919 South Place', '221-18-941', '2017-05-23 14:37:15', 1),
(45, 'Doyle', 'Rivalland', 39, '46383 Utah Drive', '462-37-186', '2017-05-23 14:37:15', 1),
(46, 'Ikey', 'Briars', 24, '51 Northridge Trail', '723-49-194', '2017-05-23 14:37:15', 1),
(47, 'Major', 'Bondley', 25, '42206 Ohio Place', '439-36-722', '2017-05-23 14:37:15', 1),
(48, 'Alejandro', 'De la croix', 48, '312 Thierer Road', '412-46-738', '2017-05-23 14:37:16', 1),
(49, 'Curtis', 'Yakovich', 33, '8 Mockingbird Crossing', '505-13-097', '2017-05-23 14:37:16', 1),
(50, 'Thor', 'Thatcher', 22, '1308 Daystar Crossing', '211-62-205', '2017-05-23 14:37:16', 1),
(51, 'Forbes', 'Boggers', 23, '17 Basil Place', '107-89-703', '2017-05-23 14:37:16', 1),
(52, 'Mordy', 'Hammerberger', 50, '4632 Carey Alley', '645-06-262', '2017-05-23 14:37:16', 1),
(53, 'Norton', 'Cashell', 47, '4 Anderson Alley', '133-14-328', '2017-05-23 14:37:16', 1),
(54, 'Lenard', 'Arndtsen', 41, '9 Kropf Pass', '646-56-735', '2017-05-23 14:37:16', 1),
(55, 'Raynard', 'Bezant', 42, '8889 Main Way', '742-81-176', '2017-05-23 14:37:16', 1),
(56, 'Lou', 'Hayter', 28, '58996 Haas Avenue', '865-14-878', '2017-05-23 14:37:16', 1),
(57, 'Gaelan', 'Vanne', 46, '1415 Bay Crossing', '111-51-604', '2017-05-23 14:37:16', 1),
(58, 'Humphrey', 'Meriguet', 49, '28718 Hauk Court', '850-67-647', '2017-05-23 14:37:16', 1),
(59, 'Ashley', 'Wynett', 43, '651 Saint Paul Street', '399-72-961', '2017-05-23 14:37:16', 1),
(60, 'Tony', 'Sole', 43, '8 Garrison Lane', '773-83-584', '2017-05-23 14:37:16', 1),
(61, 'Darn', 'Scates', 33, '3 Melby Trail', '320-71-047', '2017-05-23 14:37:16', 1),
(62, 'Vaclav', 'Wixon', 34, '9274 Cody Drive', '244-20-347', '2017-05-23 14:37:16', 1),
(63, 'Felicio', 'Guarnier', 47, '9 South Plaza', '514-61-177', '2017-05-23 14:37:16', 1),
(64, 'Antone', 'Busfield', 25, '5103 Dawn Pass', '641-82-691', '2017-05-23 14:37:16', 1),
(65, 'Micheal', 'Houseman', 46, '167 Lyons Plaza', '358-06-546', '2017-05-23 14:37:16', 1),
(66, 'Orazio', 'Ruggles', 36, '48888 Raven Lane', '327-28-401', '2017-05-23 14:37:16', 1),
(67, 'Ansel', 'Rayner', 49, '402 Surrey Plaza', '290-52-201', '2017-05-23 14:37:16', 1),
(68, 'Mar', 'Lewsy', 35, '52206 Farwell Plaza', '425-72-816', '2017-05-23 14:37:16', 1),
(69, 'Sylvan', 'Dunsire', 31, '4198 Leroy Way', '278-94-430', '2017-05-23 14:37:16', 1),
(70, 'Arlan', 'Chaster', 30, '98 Mendota Circle', '124-43-180', '2017-05-23 14:37:16', 1),
(71, 'Hogan', 'Bagwell', 28, '4836 Sunfield Hill', '294-09-822', '2017-05-23 14:37:16', 1),
(72, 'Adolf', 'Boddis', 23, '9689 Talisman Plaza', '321-66-469', '2017-05-23 14:37:16', 1),
(73, 'Stefano', 'Crosthwaite', 48, '50 Rusk Hill', '229-08-598', '2017-05-23 14:37:16', 1),
(74, 'Germayne', 'Keward', 25, '4 Monterey Hill', '145-18-877', '2017-05-23 14:37:16', 1),
(75, 'Montague', 'Goard', 30, '4175 Cottonwood Place', '332-41-441', '2017-05-23 14:37:16', 1),
(76, 'Maurizio', 'Ruhben', 45, '4 Hanson Point', '639-05-038', '2017-05-23 14:37:17', 1),
(77, 'Hew', 'Pannett', 39, '290 Westridge Street', '756-33-639', '2017-05-23 14:37:17', 1),
(78, 'Gregorius', 'Le Teve', 25, '947 Jackson Junction', '336-63-728', '2017-05-23 14:37:17', 1),
(79, 'Cesare', 'Cresser', 45, '634 Vermont Parkway', '415-07-395', '2017-05-23 14:37:17', 1),
(80, 'Myron', 'Elson', 24, '428 Surrey Way', '860-91-934', '2017-05-23 14:37:17', 1),
(81, 'Jameson', 'Dowrey', 46, '70 Kensington Drive', '591-49-954', '2017-05-23 14:37:17', 1),
(82, 'Rock', 'Rivitt', 37, '00 Upham Road', '654-35-476', '2017-05-23 14:37:17', 1),
(83, 'Krispin', 'Stanett', 31, '07291 Almo Pass', '481-53-794', '2017-05-23 14:37:17', 1),
(84, 'Carly', 'Knowles', 20, '121 Fieldstone Hill', '296-17-719', '2017-05-23 14:37:17', 1),
(85, 'Pren', 'McBride', 26, '3417 Gina Street', '226-71-909', '2017-05-23 14:37:17', 1),
(86, 'Davide', 'Moores', 37, '283 Ridgeway Drive', '291-20-877', '2017-05-23 14:37:17', 1),
(87, 'Thorstein', 'Crame', 23, '9 Delaware Way', '469-54-727', '2017-05-23 14:37:17', 1),
(88, 'Far', 'McCready', 25, '0 Scofield Terrace', '387-09-853', '2017-05-23 14:37:17', 1),
(89, 'Kennedy', 'Burwood', 46, '5361 Delaware Pass', '687-58-292', '2017-05-23 14:37:17', 1),
(90, 'Davide', 'Cossar', 36, '134 Cardinal Alley', '967-28-520', '2017-05-23 14:37:17', 1),
(91, 'Forest', 'Canelas', 34, '39944 Shelley Trail', '565-86-305', '2017-05-23 14:37:17', 1),
(92, 'Grange', 'Give', 21, '87 Nevada Avenue', '389-11-207', '2017-05-23 14:37:17', 1),
(93, 'Gil', 'Sawley', 23, '6554 Havey Avenue', '167-46-407', '2017-05-23 14:37:17', 1),
(94, 'Sanson', 'Jon', 48, '27516 Jenna Park', '272-94-764', '2017-05-23 14:37:17', 1),
(95, 'Waylon', 'Gundrey', 34, '81148 Park Meadow Park', '204-16-140', '2017-05-23 14:37:17', 1),
(96, 'Brocky', 'Blaze', 40, '9372 Warrior Avenue', '263-45-457', '2017-05-23 14:37:17', 1),
(97, 'Gage', 'Flather', 41, '08247 Scott Street', '618-52-322', '2017-05-23 14:37:17', 1),
(98, 'Nathanial', 'Lascelles', 35, '3029 Autumn Leaf Lane', '357-88-530', '2017-05-23 14:37:17', 1),
(99, 'Toby', 'Dovidian', 32, '6942 Lotheville Road', '750-38-781', '2017-05-23 14:37:17', 1),
(100, 'Nappie', 'O\'Suaird', 26, '48739 Rockefeller Point', '779-55-854', '2017-05-23 14:37:17', 1),
(101, 'Emmanuel', 'Pleavin', 28, '1263 Gina Junction', '929-25-072', '2017-05-23 14:37:17', 1),
(102, 'Pembroke', 'Spellacy', 25, '16593 West Way', '144-15-032', '2017-05-23 14:37:17', 1),
(103, 'Durant', 'Roskruge', 30, '5 Towne Junction', '428-53-937', '2017-05-23 14:37:17', 1),
(104, 'Corbin', 'Rottery', 29, '0 Center Point', '337-15-225', '2017-05-23 14:37:17', 1),
(105, 'Garv', 'Pawsey', 22, '60641 Ridge Oak Hill', '255-87-978', '2017-05-23 14:37:17', 1),
(106, 'Beale', 'Killough', 26, '18 Nova Parkway', '519-33-568', '2017-05-23 14:37:17', 1),
(107, 'Brander', 'Liveing', 33, '34066 Springview Road', '463-83-749', '2017-05-23 14:37:17', 1),
(108, 'Claiborn', 'Playfoot', 42, '87 Macpherson Terrace', '183-76-741', '2017-05-23 14:37:18', 1),
(109, 'Yuma', 'Lathleiff', 26, '26729 Scoville Avenue', '260-18-179', '2017-05-23 14:37:18', 1),
(110, 'Ban', 'FitzGeorge', 32, '7 Caliangt Junction', '347-34-769', '2017-05-23 14:37:18', 1),
(111, 'Wyatt', 'Ellgood', 38, '90814 Heffernan Road', '945-02-332', '2017-05-23 14:37:18', 1),
(112, 'Aguie', 'Davys', 46, '93069 Merry Point', '381-69-111', '2017-05-23 14:37:18', 1),
(113, 'Chucho', 'Beard', 32, '8503 Kedzie Pass', '690-01-105', '2017-05-23 14:37:18', 1),
(114, 'Andrea', 'Berford', 40, '6293 Amoth Crossing', '691-12-596', '2017-05-23 14:37:18', 1),
(115, 'Yorgos', 'Karlqvist', 43, '33345 Goodland Court', '247-64-532', '2017-05-23 14:37:18', 1),
(116, 'Pall', 'Royste', 35, '78976 Autumn Leaf Place', '284-07-371', '2017-05-23 14:37:18', 1),
(117, 'Gregor', 'Ech', 39, '61 Lillian Alley', '879-16-470', '2017-05-23 14:37:18', 1),
(118, 'Yale', 'Smallacombe', 26, '86775 Armistice Alley', '917-57-232', '2017-05-23 14:37:18', 1),
(119, 'Ephrem', 'Linham', 22, '42541 5th Hill', '701-87-469', '2017-05-23 14:37:18', 1),
(120, 'Barclay', 'Foucher', 33, '3637 Hintze Terrace', '179-33-252', '2017-05-23 14:37:18', 1),
(121, 'Gerick', 'Tick', 50, '27389 Cherokee Drive', '996-57-476', '2017-05-23 14:37:18', 1),
(122, 'Arlin', 'Yakobovitz', 35, '293 Schurz Junction', '811-54-477', '2017-05-23 14:37:18', 1),
(123, 'Normand', 'Rolin', 44, '00269 Pleasure Place', '979-62-940', '2017-05-23 14:37:18', 1),
(124, 'Ely', 'Orgel', 31, '2585 Ronald Regan Terrace', '955-54-581', '2017-05-23 14:37:18', 1),
(125, 'Julie', 'Gwinnell', 22, '2314 Magdeline Terrace', '688-92-122', '2017-05-23 14:37:18', 1),
(126, 'Monte', 'Rabjohn', 22, '1543 Reindahl Street', '526-49-168', '2017-05-23 14:37:18', 1),
(127, 'Friedrich', 'Forst', 45, '541 Shoshone Pass', '670-17-414', '2017-05-23 14:37:18', 1),
(128, 'Kalil', 'Rodmell', 44, '63518 Gulseth Road', '580-96-945', '2017-05-23 14:37:18', 1),
(129, 'Greggory', 'Sibly', 39, '88079 Lawn Park', '922-93-871', '2017-05-23 14:37:18', 1),
(130, 'Berkeley', 'Thorns', 41, '7 Columbus Center', '506-61-928', '2017-05-23 14:37:18', 1),
(131, 'Bryon', 'Acreman', 33, '386 Birchwood Circle', '845-27-247', '2017-05-23 14:37:18', 1),
(132, 'Rand', 'Bocock', 41, '38707 Coolidge Street', '329-52-436', '2017-05-23 14:37:18', 1),
(133, 'Davide', 'Umfrey', 30, '11 Summerview Way', '935-19-378', '2017-05-23 14:37:18', 1),
(134, 'Parke', 'Pringle', 44, '5 Donald Plaza', '222-60-990', '2017-05-23 14:37:18', 1),
(135, 'Grove', 'Kingham', 36, '11 Gale Avenue', '235-58-936', '2017-05-23 14:37:18', 1),
(136, 'Harper', 'Lakeland', 23, '98209 Elgar Pass', '969-60-035', '2017-05-23 14:37:18', 1),
(137, 'Gavin', 'Montfort', 41, '89 Bluestem Trail', '157-91-734', '2017-05-23 14:37:18', 1),
(138, 'Ewart', 'Maunsell', 41, '53 Holmberg Circle', '880-46-625', '2017-05-23 14:37:19', 1),
(139, 'Simeon', 'Ziemens', 22, '690 Sauthoff Terrace', '128-20-957', '2017-05-23 14:37:19', 1),
(140, 'Virgilio', 'Teasdale', 39, '27655 Burrows Avenue', '624-26-933', '2017-05-23 14:37:19', 1),
(141, 'Henry', 'Cashell', 22, '9067 John Wall Terrace', '313-97-996', '2017-05-23 14:37:19', 1),
(142, 'Hastings', 'Nilles', 46, '8666 Delladonna Park', '313-81-255', '2017-05-23 14:37:19', 1),
(143, 'Obed', 'Mackleden', 34, '8 Calypso Lane', '309-58-177', '2017-05-23 14:37:19', 1),
(144, 'Jeramie', 'Hassett', 33, '7 Bowman Circle', '499-90-730', '2017-05-23 14:37:19', 1),
(145, 'Stanleigh', 'Schust', 47, '8 Kensington Terrace', '887-46-081', '2017-05-23 14:37:19', 1),
(146, 'Hammad', 'Warton', 50, '129 Knutson Junction', '590-53-327', '2017-05-23 14:37:19', 1),
(147, 'Tobias', 'Wybern', 25, '1947 Carberry Pass', '588-12-745', '2017-05-23 14:37:19', 1),
(148, 'Nevil', 'Manass', 32, '13271 Onsgard Road', '507-67-126', '2017-05-23 14:37:19', 1),
(149, 'Nicola', 'Detoc', 50, '4 5th Parkway', '109-94-516', '2017-05-23 14:37:19', 1),
(150, 'Yurik', 'Isson', 40, '03662 Claremont Park', '811-00-540', '2017-05-23 14:37:19', 1),
(151, 'Louie', 'Guilayn', 49, '72738 Dennis Court', '857-91-854', '2017-05-23 14:37:19', 1),
(152, 'Ulises', 'Thacker', 27, '74 Gale Crossing', '435-48-442', '2017-05-23 14:37:19', 1),
(153, 'Welch', 'Coope', 38, '8965 Annamark Way', '745-41-430', '2017-05-23 14:37:19', 1),
(154, 'Adrian', 'Rippin', 34, '40 Lighthouse Bay Crossing', '809-78-009', '2017-05-23 14:37:19', 1),
(155, 'Durand', 'Hastler', 20, '886 Duke Alley', '663-00-003', '2017-05-23 14:37:19', 1),
(156, 'Stillman', 'Shear', 43, '05047 Fordem Center', '791-23-471', '2017-05-23 14:37:19', 1),
(157, 'Arther', 'Parlott', 45, '75 Sycamore Lane', '291-58-039', '2017-05-23 14:37:19', 1),
(158, 'Ellery', 'Beverage', 36, '54855 Maryland Court', '426-14-862', '2017-05-23 14:37:19', 1),
(159, 'Chickie', 'Domonkos', 37, '91 Gale Circle', '723-80-743', '2017-05-23 14:37:19', 1),
(160, 'Clayborne', 'MacIlhargy', 35, '1318 Ramsey Alley', '418-70-944', '2017-05-23 14:37:19', 1),
(161, 'Avictor', 'Chinery', 46, '0316 Evergreen Point', '428-47-195', '2017-05-23 14:37:19', 1),
(162, 'Aguie', 'Sheard', 27, '33 Westend Hill', '985-85-393', '2017-05-23 14:37:19', 1),
(163, 'Eberto', 'Gager', 37, '1 Crescent Oaks Circle', '809-05-461', '2017-05-23 14:37:19', 1),
(164, 'Briant', 'Rraundl', 38, '724 Vernon Avenue', '274-47-548', '2017-05-23 14:37:19', 1),
(165, 'Leonerd', 'MacCracken', 34, '44 Truax Plaza', '284-96-303', '2017-05-23 14:37:19', 1),
(166, 'Toby', 'Jeffery', 22, '0456 Ryan Plaza', '751-44-406', '2017-05-23 14:37:19', 1),
(167, 'Ced', 'Ilyasov', 30, '65 2nd Way', '910-42-845', '2017-05-23 14:37:19', 1),
(168, 'George', 'Zannelli', 33, '89 Homewood Pass', '229-83-765', '2017-05-23 14:37:19', 1),
(169, 'Barr', 'Beames', 40, '18 Schurz Place', '315-90-225', '2017-05-23 14:37:20', 1),
(170, 'Evan', 'Clay', 23, '83103 Bashford Circle', '648-75-097', '2017-05-23 14:37:20', 1),
(171, 'Aymer', 'Werrilow', 33, '0 Shopko Terrace', '772-54-808', '2017-05-23 14:37:20', 1),
(172, 'Domenic', 'MacMenamie', 47, '766 Mariners Cove Park', '684-83-422', '2017-05-23 14:37:20', 1),
(173, 'Keene', 'Jeandel', 22, '9481 Elka Drive', '257-57-256', '2017-05-23 14:37:20', 1),
(174, 'Caz', 'Warham', 30, '5 Algoma Parkway', '591-46-473', '2017-05-23 14:37:20', 1),
(175, 'Kirk', 'Egentan', 42, '7 Claremont Alley', '501-59-052', '2017-05-23 14:37:20', 1),
(176, 'Claudian', 'Povlsen', 24, '7002 Nobel Center', '180-43-802', '2017-05-23 14:37:20', 1),
(177, 'Kiley', 'Perrelle', 40, '592 Quincy Court', '444-33-207', '2017-05-23 14:37:20', 1),
(178, 'Adham', 'Freear', 21, '51 Trailsway Plaza', '597-24-544', '2017-05-23 14:37:20', 1),
(179, 'Ephrayim', 'Stubbings', 28, '9579 Emmet Center', '810-14-662', '2017-05-23 14:37:20', 1),
(180, 'Delaney', 'Tettley', 33, '81 Bashford Alley', '467-95-437', '2017-05-23 14:37:20', 1),
(181, 'Farly', 'Seppey', 20, '05 5th Street', '401-17-010', '2017-05-23 14:37:20', 1),
(182, 'Mattie', 'Wilflinger', 21, '0 Burning Wood Way', '117-68-141', '2017-05-23 14:37:20', 1),
(183, 'Em', 'Beavors', 35, '5584 Merry Drive', '128-40-944', '2017-05-23 14:37:20', 1),
(184, 'Jarret', 'Murrum', 35, '66374 5th Drive', '662-49-255', '2017-05-23 14:37:20', 1),
(185, 'Levey', 'Lisciandri', 43, '88539 Anderson Crossing', '429-71-489', '2017-05-23 14:37:20', 1),
(186, 'Lars', 'Nisuis', 46, '6777 Toban Center', '822-74-515', '2017-05-23 14:37:20', 1),
(187, 'Ash', 'Vlasyev', 23, '20 Kingsford Center', '693-00-940', '2017-05-23 14:37:20', 1),
(188, 'Forrester', 'Canning', 26, '<p>\r\n	0 Lakewood Gardens Way</p>\r\n', '661-67-085', '2017-05-23 14:37:20', 1),
(189, 'Toby', 'Brazel', 21, '006 Ohio Pass', '688-38-134', '2017-05-23 14:37:20', 1),
(190, 'Kipp', 'Flintuff', 25, '7 Comanche Crossing', '423-29-516', '2017-05-23 14:37:20', 1),
(191, 'Sim', 'Gethings', 47, '029 Hollow Ridge Way', '455-83-101', '2017-05-23 14:37:20', 1),
(192, 'See', 'Julian', 21, '9298 Mifflin Park', '849-83-166', '2017-05-23 14:37:20', 1),
(193, 'Ban', 'Enden', 23, '919 Hermina Court', '389-38-892', '2017-05-23 14:37:20', 1),
(194, 'Raffaello', 'Tatnell', 44, '74 Forest Run Road', '494-18-856', '2017-05-23 14:37:20', 1),
(195, 'Jordon', 'Iiannoni', 44, '639 Troy Trail', '413-97-204', '2017-05-23 14:37:20', 1),
(196, 'Sim', 'Pampling', 50, '160 High Crossing Parkway', '751-01-498', '2017-05-23 14:37:20', 1),
(197, 'Ricki', 'Karsh', 39, '04 Rutledge Point', '594-99-233', '2017-05-23 14:37:20', 1),
(198, 'Geno', 'Cromly', 42, '03 Express Plaza', '988-34-781', '2017-05-23 14:37:20', 1),
(199, 'Alonso', 'Dugmore', 37, '790 Graedel Hill', '516-28-273', '2017-05-23 14:37:20', 1),
(200, 'Anthony', 'Lapree', 44, '8 1st Circle', '620-49-307', '2017-05-29 15:55:57', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orderdocument`
--

CREATE TABLE `orderdocument` (
  `order_id` int(11) NOT NULL,
  `order_no` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order_employeeid` int(11) NOT NULL,
  `order_total` decimal(10,0) NOT NULL,
  `order_discount` decimal(10,0) NOT NULL,
  `order_promotecode` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orderdocument`
--

INSERT INTO `orderdocument` (`order_id`, `order_no`, `order_employeeid`, `order_total`, `order_discount`, `order_promotecode`, `createdDate`, `order_state`) VALUES
(1, 'PO-00000000', 0, '0', '0', '', '2017-05-29 15:56:06', 1),
(2, 'PO-10000000', 0, '9', '0', '', '2017-05-29 15:56:36', 5),
(3, 'PO-20000000', 0, '0', '0', '', '2017-05-29 16:01:05', 5),
(4, 'PO-30000000', 0, '0', '0', '', '2017-05-29 16:01:31', 5),
(5, 'PO-40000000', 0, '180', '0', '', '2017-05-29 16:02:09', 5),
(6, 'PO-50000000', 0, '1794', '0', '', '2017-05-29 16:02:51', 1),
(7, 'PO-60000000', 0, '72', '0', '', '2017-05-29 16:21:03', 5),
(8, 'PO-70000000', 0, '0', '0', '', '2017-05-29 16:22:31', 2),
(9, 'PO-80000000', 0, '0', '0', '', '2017-05-29 16:24:19', 5),
(10, 'PO-90000000', 0, '369', '0', '', '2017-05-29 16:26:33', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orderdocumentitem`
--

CREATE TABLE `orderdocumentitem` (
  `item_id` int(11) NOT NULL,
  `item_orderid` int(11) NOT NULL,
  `item_productid` int(11) NOT NULL,
  `item_qty` int(11) NOT NULL,
  `item_total` decimal(10,0) NOT NULL,
  `item_employeeid` int(11) DEFAULT NULL,
  `item_from` time NOT NULL,
  `item_to` time NOT NULL,
  `item_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orderdocumentitem`
--

INSERT INTO `orderdocumentitem` (`item_id`, `item_orderid`, `item_productid`, `item_qty`, `item_total`, `item_employeeid`, `item_from`, `item_to`, `item_date`) VALUES
(3, 5, 85, 1, '90', 8, '00:00:00', '00:00:00', '0000-00-00'),
(4, 5, 90, 1, '91', 8, '00:00:00', '00:00:00', '0000-00-00'),
(5, 6, 96, 1, '68', 8, '00:00:00', '00:00:00', '0000-00-00'),
(6, 7, 95, 1, '23', 8, '00:00:00', '00:00:00', '0000-00-00'),
(7, 8, 96, 1, '68', 8, '00:00:00', '00:00:00', '0000-00-00'),
(8, 9, 93, 1, '65', 9, '00:00:00', '00:00:00', '0000-00-00'),
(9, 10, 91, 1, '82', 10, '00:00:00', '00:00:00', '0000-00-00'),
(10, 11, 85, 1, '90', 8, '00:00:00', '00:00:00', '0000-00-00'),
(11, 12, 94, 1, '89', 9, '00:00:00', '00:00:00', '0000-00-00'),
(12, 13, 88, 1, '64', 10, '00:00:00', '00:00:00', '0000-00-00'),
(13, 14, 90, 1, '91', 6, '00:00:00', '00:00:00', '0000-00-00'),
(14, 15, 87, 1, '56', 6, '00:00:00', '00:00:00', '0000-00-00'),
(15, 16, 87, 1, '56', 8, '00:00:00', '00:00:00', '0000-00-00'),
(16, 17, 87, 1, '56', 6, '00:00:00', '00:00:00', '0000-00-00'),
(17, 17, 98, 1, '25', 10, '00:00:00', '00:00:00', '0000-00-00'),
(18, 18, 82, 1, '78', 6, '00:00:00', '00:00:00', '0000-00-00'),
(19, 19, 82, 1, '78', 7, '00:00:00', '00:00:00', '0000-00-00'),
(20, 20, 96, 1, '68', 8, '00:00:00', '00:00:00', '0000-00-00'),
(21, 21, 92, 1, '22', 9, '11:30:32', '12:30:32', '0000-00-00'),
(22, 22, 92, -1, '-22', 10, '00:00:00', '00:00:00', '0000-00-00'),
(23, 22, 94, 2, '178', 10, '00:00:00', '00:00:00', '0000-00-00'),
(24, 23, 88, 2, '128', 8, '12:30:43', '14:30:43', '0000-00-00'),
(25, 24, 82, 4, '312', 7, '08:30:57', '12:30:57', '0000-00-00'),
(26, 25, 85, -4, '-360', 16, '16:45:56', '12:45:56', '0000-00-00'),
(27, 26, 86, 0, '0', 12, '12:45:28', '12:45:28', '0000-00-00'),
(28, 27, 87, 0, '0', 28, '19:45:26', '19:45:26', '0000-00-00'),
(29, 28, 89, 3, '123', 24, '17:00:22', '20:00:22', '0000-00-00'),
(30, 29, 82, 3, '234', 30, '17:00:49', '20:00:49', '0000-00-00'),
(31, 30, 88, 0, '0', 27, '20:00:36', '20:00:36', '0000-00-00'),
(32, 31, 87, 0, '0', 32, '20:00:52', '20:00:52', '1970-01-01'),
(33, 32, 88, 2, '128', 13, '19:15:40', '21:15:40', '1970-01-01'),
(34, 33, 84, 0, '0', 33, '21:15:26', '21:15:26', '0000-00-00'),
(35, 34, 86, 0, '0', 23, '21:15:42', '21:15:42', '1970-01-01'),
(36, 35, 86, 0, '0', 19, '21:15:49', '21:15:49', '2017-05-11'),
(37, 36, 87, 0, '0', 17, '22:00:37', '22:00:37', '2017-05-29'),
(38, 37, 84, 0, '0', 200, '22:00:37', '22:00:37', '2017-05-29'),
(39, 38, 86, 3, '267', 11, '19:00:15', '22:00:15', '2017-05-29'),
(40, 39, 93, 1, '65', 14, '21:30:57', '22:30:57', '2017-05-29'),
(41, 1, 83, 0, '0', 11, '23:00:03', '23:00:03', '2017-05-29'),
(42, 2, 83, 1, '9', 12, '22:00:34', '23:00:34', '2017-05-31'),
(43, 3, 83, 0, '0', 12, '23:00:57', '23:00:57', '2017-05-29'),
(44, 4, 83, 0, '0', 12, '23:15:24', '23:15:24', '2017-05-31'),
(45, 5, 83, 20, '180', 12, '03:15:05', '23:15:05', '2017-05-29'),
(46, 6, 82, 23, '1794', 12, '00:15:48', '23:15:48', '2017-05-29'),
(47, 7, 83, 8, '72', 11, '15:30:58', '23:30:58', '2017-06-01'),
(48, 8, 84, 0, '0', 13, '23:30:47', '23:30:47', '2017-05-29'),
(49, 9, 87, 0, '0', 15, '23:30:18', '23:30:18', '2017-05-29'),
(50, 10, 89, 9, '369', 14, '00:30:58', '09:30:58', '2017-05-29');

-- --------------------------------------------------------

--
-- Stand-in structure for view `printfrom`
-- (See below for the actual view)
--
CREATE TABLE `printfrom` (
`product_name` varchar(500)
,`product_price` decimal(18,8)
,`order_no` varchar(100)
,`order_id` int(11)
,`CreateDocument` timestamp
,`item_qty` int(11)
,`item_total` decimal(10,0)
,`employee_id` int(11)
,`employee_fname` varchar(50)
,`employee_lname` varchar(50)
,`employee_age` int(11)
,`employee_address` text
,`employee_tel` varchar(10)
,`createdDate` timestamp
,`IsActive` tinyint(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `product_producttypeid` int(11) NOT NULL,
  `product_description` text COLLATE utf8_unicode_ci NOT NULL,
  `product_price` decimal(18,8) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `product_producttypeid`, `product_description`, `product_price`, `createdDate`) VALUES
(82, 'Levofloxacin', 1, 'Universidad Nacional de la Amazonía Peruana', '78.00000000', '2017-05-23 14:21:21'),
(83, 'tolterodine tartrate', 1, 'Universidad San Marcos', '9.00000000', '2017-05-23 14:21:21'),
(84, 'Spongia Ovi', 1, 'American InterContinental University - Ft. Lauderdale', '78.00000000', '2017-05-23 14:21:21'),
(85, 'Acetaminophen', 1, 'North Bengal University', '90.00000000', '2017-05-23 14:21:21'),
(86, 'PANTOPRAZOLE SODIUM', 1, 'Turkish Naval Academy', '89.00000000', '2017-05-23 14:21:21'),
(87, 'ATORVASTATIN CALCIUM', 1, 'Sampurnanand Sanskrit University', '56.00000000', '2017-05-23 14:21:21'),
(88, 'TRICON', 1, 'Rostov State Medical University', '64.00000000', '2017-05-23 14:21:21'),
(89, 'acetaminophen', 1, 'Universidade Federal de Ouro Preto', '41.00000000', '2017-05-23 14:21:21'),
(90, 'lisdexamfetamine dimesylate', 1, 'Diyala University', '91.00000000', '2017-05-23 14:21:21'),
(91, 'Zolpidem Tartrate', 1, 'Shaikh Zayed University', '82.00000000', '2017-05-23 14:21:21'),
(92, 'Naproxen Sodium', 1, 'Universidad de Magallanes', '22.00000000', '2017-05-23 14:21:21'),
(93, 'nitroglycerin', 1, 'American University of Sharjah', '65.00000000', '2017-05-23 14:21:22'),
(94, 'Warfarin Sodium', 1, 'Doho University', '89.00000000', '2017-05-23 14:21:22'),
(95, 'tamsulosin hydrochloride', 1, 'Pontificia Università Lateranense', '23.00000000', '2017-05-23 14:21:22'),
(96, 'Promethazinehydrochloride and phenylephrine hydrochloride', 1, 'Suan Dusit Rajabhat University', '68.00000000', '2017-05-23 14:21:22'),
(97, 'Loperamide Hydrochloride', 1, 'University of the Pacific', '40.00000000', '2017-05-23 14:21:22'),
(98, 'Octinoxate, Zinc Oxide', 1, 'Thompson Rivers University', '25.00000000', '2017-05-23 14:21:22'),
(99, 'XXXX', 2, '', '500.00000000', '2017-05-29 12:42:20');

-- --------------------------------------------------------

--
-- Stand-in structure for view `productsummaryreport`
-- (See below for the actual view)
--
CREATE TABLE `productsummaryreport` (
`order_id` int(11)
,`order_no` varchar(100)
,`CreateDocument` timestamp
,`item_qty` int(11)
,`item_total` decimal(10,0)
,`product_name` varchar(500)
,`product_price` decimal(18,8)
,`employee_id` int(11)
,`employee_fname` varchar(50)
,`employee_lname` varchar(50)
,`employee_age` int(11)
,`employee_address` text
,`employee_tel` varchar(10)
,`createdDate` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `producttype`
--

CREATE TABLE `producttype` (
  `producttype_id` int(11) NOT NULL,
  `producttype_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `producttype`
--

INSERT INTO `producttype` (`producttype_id`, `producttype_name`, `createdDate`) VALUES
(1, ' Massage Service', '2017-05-22 14:36:55'),
(2, 'Beauty products', '2017-05-22 14:47:33');

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `promo_id` int(11) NOT NULL,
  `promo_description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `promo_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`promo_id`, `promo_description`, `promo_code`, `createdDate`) VALUES
(1, 'ZEN SHOES GRAND SALE ลดสูงสุด 70% ที่ห้างสรรพสินค้าเซน (วันนี้ - 4 มิ.ย. 2560) โอกาสเดียวเท่านั้น กับงานเซลรองเท้าและกระเป๋าครั้งสำคัญที่คุณห้ามพลาด \"ZEN SHOES GRAND SALE\" สินค้าคุณภาพลดสูงสุด 70% - รับส่วนลดเพิ่ม 15% เมื่อใช้คะแนน The1Card (วันที่ 25 - 31 พ.ค. 60 จากรายการ The 1 Card X-Treme ll ) - รับส่วนลดเพิ่ม 12.5% เมื่อใช้คะแนน The1Card (วันที่ 1 - 4 มิ.ย. 60 จากรายการปกติ', 'Hello2017', '2017-05-24 17:00:00');

-- --------------------------------------------------------

--
-- Stand-in structure for view `servicesummaryreport`
-- (See below for the actual view)
--
CREATE TABLE `servicesummaryreport` (
`producttype_id` int(11)
,`producttype_name` varchar(100)
,`CreateDocument` timestamp
,`Item_Qty` decimal(32,0)
,`Item_Total` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_role` int(11) NOT NULL,
  `user_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_role`, `user_name`, `user_password`, `createdDate`) VALUES
(1, 100, 'admin', '1234', '2017-05-23 13:18:11');

-- --------------------------------------------------------

--
-- Structure for view `dairysummaryreport`
--
DROP TABLE IF EXISTS `dairysummaryreport`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dairysummaryreport`  AS  select `orders`.`createdDate` AS `CreateDocument`,sum(`ordersitem`.`item_qty`) AS `Item_Qty`,sum(`ordersitem`.`item_total`) AS `Item_Total`,`employee`.`employee_id` AS `employee_id`,`employee`.`employee_fname` AS `employee_fname`,`employee`.`employee_lname` AS `employee_lname`,`employee`.`employee_age` AS `employee_age`,`employee`.`employee_address` AS `employee_address`,`employee`.`employee_tel` AS `employee_tel`,`employee`.`createdDate` AS `createdDate` from ((`orderdocument` `orders` join `orderdocumentitem` `ordersitem` on((`orders`.`order_id` = `ordersitem`.`item_orderid`))) join `employee` on((`employee`.`employee_id` = `ordersitem`.`item_employeeid`))) group by `ordersitem`.`item_employeeid` ;

-- --------------------------------------------------------

--
-- Structure for view `printfrom`
--
DROP TABLE IF EXISTS `printfrom`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `printfrom`  AS  select `product`.`product_name` AS `product_name`,`product`.`product_price` AS `product_price`,`orders`.`order_no` AS `order_no`,`orders`.`order_id` AS `order_id`,`orders`.`createdDate` AS `CreateDocument`,`ordersitem`.`item_qty` AS `item_qty`,`ordersitem`.`item_total` AS `item_total`,`employee`.`employee_id` AS `employee_id`,`employee`.`employee_fname` AS `employee_fname`,`employee`.`employee_lname` AS `employee_lname`,`employee`.`employee_age` AS `employee_age`,`employee`.`employee_address` AS `employee_address`,`employee`.`employee_tel` AS `employee_tel`,`employee`.`createdDate` AS `createdDate`,`employee`.`IsActive` AS `IsActive` from (((`orderdocument` `orders` join `orderdocumentitem` `ordersitem` on((`orders`.`order_id` = `ordersitem`.`item_orderid`))) join `employee` on((`employee`.`employee_id` = `ordersitem`.`item_employeeid`))) join `product` on((`product`.`product_id` = `ordersitem`.`item_productid`))) ;

-- --------------------------------------------------------

--
-- Structure for view `productsummaryreport`
--
DROP TABLE IF EXISTS `productsummaryreport`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `productsummaryreport`  AS  select `orders`.`order_id` AS `order_id`,`orders`.`order_no` AS `order_no`,`orders`.`createdDate` AS `CreateDocument`,`ordersitem`.`item_qty` AS `item_qty`,`ordersitem`.`item_total` AS `item_total`,`product`.`product_name` AS `product_name`,`product`.`product_price` AS `product_price`,`employee`.`employee_id` AS `employee_id`,`employee`.`employee_fname` AS `employee_fname`,`employee`.`employee_lname` AS `employee_lname`,`employee`.`employee_age` AS `employee_age`,`employee`.`employee_address` AS `employee_address`,`employee`.`employee_tel` AS `employee_tel`,`employee`.`createdDate` AS `createdDate` from (((`orderdocument` `orders` join `orderdocumentitem` `ordersitem` on((`orders`.`order_id` = `ordersitem`.`item_orderid`))) join `employee` on((`employee`.`employee_id` = `ordersitem`.`item_employeeid`))) join `product` on((`product`.`product_id` = `ordersitem`.`item_productid`))) ;

-- --------------------------------------------------------

--
-- Structure for view `servicesummaryreport`
--
DROP TABLE IF EXISTS `servicesummaryreport`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `servicesummaryreport`  AS  select `prtype`.`producttype_id` AS `producttype_id`,`prtype`.`producttype_name` AS `producttype_name`,`orders`.`createdDate` AS `CreateDocument`,sum(`ordersitem`.`item_qty`) AS `Item_Qty`,sum(`ordersitem`.`item_total`) AS `Item_Total` from ((((`orderdocument` `orders` join `orderdocumentitem` `ordersitem` on((`orders`.`order_id` = `ordersitem`.`item_orderid`))) join `employee` on((`employee`.`employee_id` = `ordersitem`.`item_employeeid`))) join `product` on((`product`.`product_id` = `ordersitem`.`item_productid`))) join `producttype` `prtype` on((`product`.`product_producttypeid` = `prtype`.`producttype_id`))) group by `prtype`.`producttype_id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `orderdocument`
--
ALTER TABLE `orderdocument`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `orderdocumentitem`
--
ALTER TABLE `orderdocumentitem`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `producttype`
--
ALTER TABLE `producttype`
  ADD PRIMARY KEY (`producttype_id`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`promo_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;
--
-- AUTO_INCREMENT for table `orderdocument`
--
ALTER TABLE `orderdocument`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `orderdocumentitem`
--
ALTER TABLE `orderdocumentitem`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `producttype`
--
ALTER TABLE `producttype`
  MODIFY `producttype_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `promo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
